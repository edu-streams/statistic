import Container from "@mui/material/Container";

const EventPageBlock = () => {
  return (
    <Container
      sx={{ height: "100vh", width: "100vw", paddingTop: "70px" }}
    ></Container>
  );
};

export default EventPageBlock;
