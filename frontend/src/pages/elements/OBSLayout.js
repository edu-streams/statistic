import {useEffect, useState} from "react";
import {socket} from "../../services/socket";
import Typography from "@mui/material/Typography";
import {Box} from "@mui/material";

const OBSLayout = () =>{

    const [message, setMessage] = useState("");

    const [isDisplay, setIsDisplay] = useState(true);

    useEffect(() => {
        socket.on('message', (msg) => {
            setMessage(msg);
        });

        socket.on('control', (control) => {
            setIsDisplay(control);
            console.log(control)
        });

        //
        // return () => {
        //     socket.off('message');
        // };
    }, []);

    return (
        <>
            <Typography>
                OBS Layout
            </Typography>
            <Box display={isDisplay}>
                <div>{message}</div>
            </Box>

        </>
    );
}
export default OBSLayout;