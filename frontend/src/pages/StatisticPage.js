import StudentsCount from "components/StudentsCount/StudentsCount";
import DrawsResults from "components/DrawsResults/DrawsResults";
import ReactionsChart from "components/ReactionsChart/ReactionsChart";
import { useSelector } from "react-redux";
import { selectDisplayed } from "redux/displayed/selectors";
import { Grid, styled } from "@mui/material";

const Item = styled("div")(({ theme }) => ({
  textAlign: "center",
  border: "solid 1px black",
}));

export default function StatisticPage() {
  const stream = useSelector(selectDisplayed);
  console.log("displayed: ", stream);

  return (
    <>
      {stream.displayed === "Оберіть стрім" && <p>Оберіть стрім зі списку</p>}
      {stream.displayed !== "Оберіть стрім" && (
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Item>
              <StudentsCount text="Були присутні, чол." />
            </Item>
          </Grid>
          <Grid item xs={6}>
            <Item
              sx={{
                height: "100%",
                maxheight: "180px",
              }}
            >
              <DrawsResults text="Переможці розіграшу" />
            </Item>
          </Grid>
          <Grid item xs={6}>
            <Item>
              {/* <StatItemHeader text="Середній бал від слухачів" /> */}
            </Item>
          </Grid>
          <Grid item xs={6}>
            <Item>
              {/* <StatItemHeader text="Результати опитування/кахуту" /> */}
            </Item>
          </Grid>
          <Grid item xs={6}>
            <Item>{/* <StatItemHeader text="Динаміка присутності" /> */}</Item>
          </Grid>
          <Grid item xs={6}>
            <Item>
              <ReactionsChart text="Динаміка активності" />
            </Item>
          </Grid>
        </Grid>
      )}
    </>
  );
}
