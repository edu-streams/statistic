export const createDataForReactions = (start, end, reactions) => {
  const startDate = new Date(start);
  const endDate = new Date(end);
  const streamLength = (endDate - startDate) / 60000;
  const segment = streamLength > 60 ? 10 : 5;
  const countOfTimeSegments = Math.round(streamLength / segment);

  const reactionsData = [];

  for (let i = 0; i < countOfTimeSegments; i += 1) {
    const data = {
      segment: `${i * 5} - ${i * 5 + 5} хв`,
      from: i * 5,
      to: i * 5 + 5,
      like: 0,
      norm: 0,
      disLike: 0,
    };

    reactionsData.push(data);
  }

  Object.keys(reactions).forEach((key) => {
    reactions[key].forEach((date) => {
      const reactionTime = (date - startDate) / 60000;

      reactionsData.forEach((data) => {
        if (reactionTime >= data.from && reactionTime < data.to) data[key] += 1;
      });
    });
  });

  return reactionsData;
};
