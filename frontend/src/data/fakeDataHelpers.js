import { faker } from "@faker-js/faker";

const createFakeStatus = () => {
  const status = ["guest", "student"];
  const userStatus = faker.datatype.number({
    min: 0,
    max: 1,
    precision: 1,
  });

  return status[userStatus];
};

const watchingTime = (start, end) => {
  const connectDate = faker.date.between(start, end);
  const disConnectDate = faker.date.between(connectDate, end);

  return { connectDate, disConnectDate };
};

const createFakeNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

const createReactions = (start, end) => {
  const count = createFakeNumber(10, 50);
  const reactions = [];

  for (let i = 0; i <= count; i += 1) {
    const time = new Date(faker.date.between(start, end)).getTime();

    reactions.push(time);
  }

  return reactions;
};

export const fakeDataFunc = {
  createFakeStatus,
  watchingTime,
  createFakeNumber,
  createReactions,
};
