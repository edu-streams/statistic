import { fakeDataFunc } from "./fakeDataHelpers";
import { faker } from "@faker-js/faker";

export const fakeViewers = [
  {
    name: "Deion",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "1",
  },
  {
    name: "Elenor",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },

      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "2",
  },
  {
    name: "Rubye",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "3",
  },
  {
    name: "Yasmeen",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "4",
  },
  {
    name: "Reid",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },

      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "5",
  },
  {
    name: "Michale",
    streams: [
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "6",
  },
  {
    name: "Litzy",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "7",
  },
  {
    name: "Murray",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "8",
  },
  {
    name: "Barry",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "9",
  },
  {
    name: "Jackson",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "10",
  },
  {
    name: "Teresa",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "11",
  },
  {
    name: "Iva",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },

      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "12",
  },
  {
    name: "Alfonso",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "13",
  },
  {
    name: "Cletus",
    streams: [
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "14",
  },
  {
    name: "Vicenta",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "15",
  },
  {
    name: "Shayne",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "16",
  },
  {
    name: "Sam",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },

      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "17",
  },
  {
    name: "Tyrique",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "18",
  },
  {
    name: "Madison",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "19",
  },
  {
    name: "Gretchen",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "20",
  },
  {
    name: "Lauryn",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "21",
  },
  {
    name: "Clemens",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "22",
  },
  {
    name: "Connor",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },

      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "23",
  },
  {
    name: "Lonzo",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "24",
  },
  {
    name: "Brooke",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "25",
  },
  {
    name: "Claud",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "26",
  },
  {
    name: "Roosevelt",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "27",
  },
  {
    name: "Pink",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },

      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "28",
  },
  {
    name: "Alvera",
    streams: [
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "29",
  },
  {
    name: "Alexys",
    streams: [
      {
        id: 1,
        viewTime: fakeDataFunc.watchingTime(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 2,
        viewTime: fakeDataFunc.watchingTime(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
      {
        id: 3,
        viewTime: fakeDataFunc.watchingTime(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
        mark: fakeDataFunc.createFakeNumber(1, 5),
        surveyAssessment: `${fakeDataFunc.createFakeNumber(50, 100)} %`,
      },
    ],
    email: "fake@mail.com",
    status: fakeDataFunc.createFakeStatus(),
    avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
    id: "30",
  },
];

export const fakeStreamData = [
  {
    id: 1,
    streamUrl: "https://www.youtube.com/watch?v=eUgpFZvdyzQ&list=LL&index=14",
    topic: "Cigarettes After Sex, Zubi, Edmofo, Carla Morrison",
    startAt: "2020-01-01T00:00:00.000Z",
    endAt: "2020-01-01T03:38:22.000Z",
    winner: [
      {
        name: "Carmelo",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "5",
        date: faker.date.between(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
      },
      {
        name: "Karina",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "28",
        date: faker.date.between(
          "2020-01-01T00:00:00.000Z",
          "2020-01-01T03:38:22.000Z"
        ),
      },
    ],
    reactions: {
      like: fakeDataFunc.createReactions(
        "2020-01-01T00:00:00.000Z",
        "2020-01-01T03:38:22.000Z"
      ),
      disLike: fakeDataFunc.createReactions(
        "2020-01-01T00:00:00.000Z",
        "2020-01-01T03:38:22.000Z"
      ),
      norm: fakeDataFunc.createReactions(
        "2020-01-01T00:00:00.000Z",
        "2020-01-01T03:38:22.000Z"
      ),
    },
  },
  {
    id: 2,
    streamUrl: "https://www.youtube.com/watch?v=0MjsxTFeuEM&list=LL&index=8",
    topic: "Море. Битва за острів Зміїний",
    startAt: "2022-01-01T00:00:00.000Z",
    endAt: "2022-01-01T00:46:17.000Z",
    winner: [
      {
        name: "Rubye",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "3",
        date: faker.date.between(
          "2022-01-01T00:00:00.000Z",
          "2022-01-01T00:46:17.000Z"
        ),
      },
    ],
    reactions: {
      like: fakeDataFunc.createReactions(
        "2022-01-01T00:00:00.000Z",
        "2022-01-01T00:46:17.000Z"
      ),
      disLike: fakeDataFunc.createReactions(
        "2022-01-01T00:00:00.000Z",
        "2022-01-01T00:46:17.000Z"
      ),
      norm: fakeDataFunc.createReactions(
        "2022-01-01T00:00:00.000Z",
        "2022-01-01T00:46:17.000Z"
      ),
    },
  },
  {
    id: 3,
    streamUrl: "https://www.youtube.com/watch?v=fsmsmANPYjk",
    topic: "Документальний фільм про оборону Харкова",
    startAt: "2023-01-01T00:00:00.000Z",
    endAt: "2023-01-01T00:54:09.000Z",
    winner: [
      {
        name: "Katlynn",

        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "20",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
      {
        name: "Hilma",

        email: "fake@mail.com",
        status: fakeDataFunc.createFakeStatus(),
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "15",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
      {
        name: "Dallin",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "30",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
      {
        name: "Van",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "7",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
      {
        name: "Kody",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "9",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
      {
        name: "Estevan",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "12",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
      {
        name: "Aimee",
        email: "fake@mail.com",
        avatar: `https://robohash.org/${fakeDataFunc.watchingTime()}?set=set4`,
        id: "16",
        date: faker.date.between(
          "2023-01-01T00:00:00.000Z",
          "2023-01-01T00:54:09.000Z"
        ),
      },
    ],
    reactions: {
      like: fakeDataFunc.createReactions(
        "2023-01-01T00:00:00.000Z",
        "2023-01-01T00:54:09.000Z"
      ),
      disLike: fakeDataFunc.createReactions(
        "2023-01-01T00:00:00.000Z",
        "2023-01-01T00:54:09.000Z"
      ),
      norm: fakeDataFunc.createReactions(
        "2023-01-01T00:00:00.000Z",
        "2023-01-01T00:54:09.000Z"
      ),
    },
  },
];
