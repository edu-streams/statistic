import { createSlice } from "@reduxjs/toolkit";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

export const persistConfig = {
  key: "displayed",
  storage,
};

export const displayedInitialState = { displayed: "Оберіть стрім", id: "" };

const displayedSlice = createSlice({
  name: "displayed",
  initialState: displayedInitialState,
  reducers: {
    setDisplayed(_, action) {
      return action.payload;
    },
  },
});

export const { setDisplayed } = displayedSlice.actions;
export const displayedReducer = persistReducer(
  persistConfig,
  displayedSlice.reducer
);
