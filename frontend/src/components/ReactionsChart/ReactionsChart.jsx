import StatItemHeader from "components/StatItemHeader/StatItemHeader";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectDisplayed } from "redux/displayed/selectors";
import { fakeStreamData } from "data/statData";
import { createDataForReactions } from "../../helpers/createDataForReactions";

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Legend,
} from "recharts";

export default function ReactionsChart({ text }) {
  const stream = useSelector(selectDisplayed);
  const [reactionsData, setReactionsData] = useState(null);

  useEffect(() => {
    const streamData = fakeStreamData.filter((data) => data.id === stream.id);
    const data = createDataForReactions(
      streamData[0].startAt,
      streamData[0].endAt,
      streamData[0].reactions
    );

    setReactionsData(data);
  }, [stream]);

  return (
    <div style={{ width: "100%" }}>
      <StatItemHeader title={text} />

      <ResponsiveContainer width="100%" height={200}>
        <LineChart
          width={500}
          height={200}
          data={reactionsData}
          syncId="anyId"
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 0,
          }}
        >
          {/* <CartesianGrid strokeDasharray="3 3" /> */}
          <XAxis dataKey="segment" />
          <YAxis />
          <Tooltip />
          <Legend verticalAlign="top" height={18} iconType="circle" />
          <Line
            name="norm"
            type="monotone"
            dataKey="norm"
            stroke="#8884d8"
            fill="#8884d8"
          />

          {/* <CartesianGrid strokeDasharray="3 3" /> */}
          <XAxis dataKey="segment" />
          <YAxis />
          <Tooltip />
          <Line
            name="like"
            type="monotone"
            dataKey="like"
            stroke="#82ca9d"
            fill="#82ca9d"
          />

          {/* <CartesianGrid strokeDasharray="3 3" /> */}
          <XAxis dataKey="segment" />
          <YAxis />
          <Tooltip />
          <Line
            name="disLike"
            type="monotone"
            dataKey="disLike"
            stroke="#ed221b"
            fill="#ed221b"
          />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}
