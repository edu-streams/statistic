import StatItemHeader from "components/StatItemHeader/StatItemHeader";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectDisplayed } from "redux/displayed/selectors";
import { fakeStreamData } from "data/statData";
import { transformDate } from "helpers/transformDateFunc";

import {
  WinnersList,
  Item,
  NameWrapper,
  AvatarWrapper,
  Avatar,
  Text,
  DateWrapper,
} from "./DrawsResults.styled";

export default function DrawsResults({ text }) {
  const stream = useSelector(selectDisplayed);
  const [winners, setWinners] = useState(null);

  useEffect(() => {
    const streamData = fakeStreamData.filter((data) => data.id === stream.id);

    if (streamData[0].winner) {
      setWinners(streamData[0].winner);
    }
  }, [stream]);

  if (!winners) return;

  const steamWinners = winners.map((win) => {
    const date = transformDate(win.date);
    return (
      <Item key={win.id}>
        <NameWrapper>
          <AvatarWrapper>
            <Avatar src={win.avatar} alt={win.name} />
          </AvatarWrapper>
          <Text>{win.name}</Text>
        </NameWrapper>
        <DateWrapper sx={{ justifySelf: "center" }}>
          <Text>{date}</Text>
        </DateWrapper>
      </Item>
    );
  });

  return (
    <>
      <StatItemHeader title={text} />
      {!winners && <p>На цьому стрімі не проводились розіграші</p>}
      {winners && <WinnersList>{steamWinners}</WinnersList>}
    </>
  );
}
