import styled from "@emotion/styled";
import { Grid } from "@mui/material";
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Box,
} from "@mui/material";

const ItemWrapper = styled(Grid)`
  height: 100%;
  max-height: 180px;
`;

const WinnersList = styled(List)`
  max-height: 145px;

  overflow-y: scroll;
  ::-webkit-scrollbar {
    width: 4px;
  }
  ::-webkit-scrollbar-thumb {
    background-color: rgba(158, 158, 158, 1);
    border-radius: 2px;
  }
`;

const Item = styled(ListItem)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 5px;

  padding-top: 0;
  padding-bottom: 0;
`;

const NameWrapper = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 25px;
`;

const AvatarWrapper = styled(ListItemAvatar)``;

const Avatar = styled.img`
  width: 30px;
  height: 30px;

  border-radius: 50%;
`;

const Text = styled(ListItemText)``;

const DateWrapper = styled(Box)``;

export {
  ItemWrapper,
  WinnersList,
  Item,
  NameWrapper,
  AvatarWrapper,
  Avatar,
  Text,
  DateWrapper,
};
