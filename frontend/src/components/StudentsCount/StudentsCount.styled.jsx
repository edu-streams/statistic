import styled from "@emotion/styled";
import { Grid, Typography } from "@mui/material";

const Wrapper = styled(Grid)`
  align-items: center;
  justify-content: center;
  gap: 20px;

  padding: 20px 0;

  max-height: 145px;

  overflow-y: scroll;
  ::-webkit-scrollbar {
    width: 4px;
  }
  ::-webkit-scrollbar-thumb {
    background-color: rgba(158, 158, 158, 1);
    border-radius: 2px;
  }
`;

const Item = styled(Grid)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 5px;

  min-width: 130px;
  padding: 15px;

  border: 1px solid #000;
  border-radius: 3px;
`;

const Count = styled(Typography)`
  margin: 0;

  font-size: 32px;
  font-weight: 600;
`;

const Label = styled(Typography)`
  margin: 0;

  font-weight: 600;
`;

export { Wrapper, Item, Count, Label };
