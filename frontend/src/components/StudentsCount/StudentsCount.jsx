import StatItemHeader from "components/StatItemHeader/StatItemHeader";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectDisplayed } from "redux/displayed/selectors";
import { fakeViewers } from "data/statData";

import { Wrapper, Item, Count, Label } from "./StudentsCount.styled";

export default function StudentsCount({ text }) {
  const streamData = useSelector(selectDisplayed);
  const [viewers, setViewers] = useState(null);

  useEffect(() => {
    const users = fakeViewers.reduce(
      (acc, user) => {
        user.streams.map(({ id }) => {
          if (id === streamData.id) {
            acc[user.status] += 1;

            return acc;
          }
        });
        return acc;
      },
      { student: 0, guest: 0 }
    );

    setViewers(users);
  }, [streamData, fakeViewers]);

  if (!viewers) return;

  return (
    <>
      <StatItemHeader title={text} />
      <Wrapper container>
        <Item item xs={2}>
          <Count>{viewers.student + viewers.guest}</Count>
          <Label>Всього</Label>
        </Item>
        <Item item xs={2}>
          <Count>{viewers.student}</Count>
          <Label>Зареєстровані</Label>
        </Item>
        <Item item xs={2}>
          <Count>{viewers.guest}</Count>
          <Label>Незареєстровані</Label>
        </Item>
      </Wrapper>
    </>
  );
}
