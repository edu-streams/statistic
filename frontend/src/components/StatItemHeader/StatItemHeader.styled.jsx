import styled from "@emotion/styled";
import { Box, Typography } from "@mui/material";

const Header = styled(Box)`
  padding: 5px 0;

  border-bottom: 1px solid #ccc;
`;

const Title = styled(Typography)`
  margin: 0;

  text-align: left;
`;

export { Header, Title };
