import { Header, Title } from "./StatItemHeader.styled";

export default function StatItemHeader({ title }) {
  return (
    <Header>
      <Title variant="h5">{title}</Title>
    </Header>
  );
}
